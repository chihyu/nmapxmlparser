import xml.etree.ElementTree as ET
import operator
import sys
if len(sys.argv)<2:
 print " No argument"
 print ""
 print " Usage: python nmapXmlParser.py <NmapXmlFile>\n"
 sys.exit()
tree =ET.parse(sys.argv[1])
root=tree.getroot()
dict={}
for host in root.findall('host'):
 portList=[]
 for address in host.findall('address'):
   ip=address.get('addr')
   if address.get('addrtype')=="ipv4":
    realip=ip
#    print ip
 for portInfo in host.findall('ports'):
  for port in  portInfo.findall('port'):
   allport=portInfo.findall('port')
   port=port.get('portid')
   portList.append(port)
#   print port
 portNum=len(portList)
 dict.update({realip:(portNum,portList)})
# print "Total:"+str(len(portList))+"\n"
sort_dict=sorted(dict.items(),key=lambda x:x[1][0],reverse=True)
for key,value in sort_dict:
 print key+" "+str(value)
